import { mount } from '@vue/test-utils'
import  listTask  from "@/components/listTask"



test('list\'s length must be equal to 0',  () => {
    const wrapper = mount(listTask)
    const list = wrapper.findAll('task')
    expect(list.length).toBe(0)
})

test('h4 tag\'s text must be equal to \'Your Todos\'', () => {
    const wrapper = mount(listTask)
    const title = wrapper.find('.listTask .title')
    expect(title.text()).toBe("Your Todos")
})



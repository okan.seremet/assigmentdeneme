package handler_test

import (
	"backend/handler"
	"backend/model"
	"backend/service"
	"encoding/json"

	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func Test_AddTodo(t *testing.T) {
	url := "/addTodo"
	e := echo.New()
	req, _ := http.NewRequest(http.MethodPost, url, nil)
	req.Header.Set("Content-Type", "application/json")

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	res := rec.Result()
	defer res.Body.Close()

	service := service.NewService()
	todo := model.Todo{ID: 1, Text: "Task 2"}
	service.AddTodo(&todo)
	handler := handler.NewHandler(service)

	if assert.NoError(t, handler.GetTodos(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, toString(todo), rec.Body.String())
	}
}

func Test_GetTodo(t *testing.T) {
	url := "/listTask"
	e := echo.New()
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("Content-Type", "application/json")

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	res := rec.Result()
	defer res.Body.Close()

	service := service.NewService()
	handler := handler.NewHandler(service)

	if assert.NoError(t, handler.AddTodo(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func toString(todo model.Todo) string {
	str, _ := json.Marshal(todo)
	return "[" + string(str) + "]\n"
}

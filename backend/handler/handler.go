package handler

import (
	"backend/model"
	"backend/service"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Handler interface {
	GetTodos(echo.Context) error
	AddTodo(echo.Context) error
}

type HandlerImpl struct {
	service service.Service
}

func NewHandler(service service.Service) Handler {
	return HandlerImpl{
		service: service,
	}
}

func (h HandlerImpl) GetTodos(c echo.Context) error {
	todos := h.service.GetTodos()
	return c.JSON(http.StatusOK, todos)
}

func (h HandlerImpl) AddTodo(c echo.Context) error {
	todo := new(model.Todo)
	err := c.Bind(todo)
	if err != nil {
		return err
	}
	h.service.AddTodo(todo)
	return c.NoContent(http.StatusCreated)
}

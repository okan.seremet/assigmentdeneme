package main

import (
	"backend/handler"
	"backend/service"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	service := service.NewService()
	handler := handler.NewHandler(service)
	e.GET("/listTodo", handler.GetTodos)
	e.POST("/addTodo", handler.AddTodo)

	e.Logger.Fatal(e.Start(":1323"))
}

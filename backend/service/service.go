package service

import "backend/model"

var TodoArray []model.Todo

type Service struct {
}

func NewService() Service {
	return Service{}
}

func (s Service) GetTodos() []model.Todo {
	return TodoArray
}

func (s Service) AddTodo(todo *model.Todo) error {
	TodoArray = append(TodoArray, *todo)
	return nil
}
